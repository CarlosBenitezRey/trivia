package com.example.pruebafragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.Navigation

class Pregunta3 : Fragment() {
    private lateinit var botonfal3: Button
    private lateinit var botonver3: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding= inflater.inflate(R.layout.fragment_pregunta3, container, false)
        botonfal3= binding.findViewById(R.id.button)
        botonver3= binding.findViewById(R.id.button2)
        botonfal3.setOnClickListener{ view ->
            Navigation.findNavController(view).navigate(R.id.action_pregunta3_to_fragmentofinal)
            Toast.makeText(activity, "Respuesta Incorrecta", Toast.LENGTH_SHORT).show()

        }
        botonver3.setOnClickListener{ view->
            Navigation.findNavController(view).navigate(R.id.action_pregunta3_to_fragmentofinal)
            Toast.makeText(activity, "Respuesta Correcta", Toast.LENGTH_SHORT).show()
        }
        return binding
    }

}