package com.example.pruebafragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import android.content.Intent
import android.widget.ImageButton

class fragmentofinal : Fragment() {
    private lateinit var botonfinal: Button
    private lateinit var botoncompartir:ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding= inflater.inflate(R.layout.fragment_fragmentofinal, container, false)
        botonfinal= binding.findViewById(R.id.botonfinal)
        botoncompartir=binding.findViewById(R.id.imageButton)
        botonfinal.setOnClickListener{ view ->
            Navigation.findNavController(view).navigate(R.id.action_fragmentofinal_to_pregunta1Fragment)
        }
        botoncompartir.setOnClickListener{
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, "Realice con exito toda la trivia")
            shareIntent
            startActivity(shareIntent)
        }
        return binding
    }

}