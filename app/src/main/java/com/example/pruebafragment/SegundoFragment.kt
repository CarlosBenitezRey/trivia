package com.example.pruebafragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.Navigation

class SegundoFragment : Fragment() {
    private lateinit var botonfal2: Button
    private lateinit var botonver2: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding= inflater.inflate(R.layout.fragment_segundo, container, false)
        botonfal2= binding.findViewById(R.id.botonfalso2)
        botonver2= binding.findViewById(R.id.botonverdadero2)

        //arguments
        arguments?.let {
            var args =Pregunta1FragmentArgs.fromBundle(it)
            Toast.makeText(context,
                "Numero de preguntas correctas: ${args.cont}",
                Toast.LENGTH_LONG).show()
        }

        botonfal2.setOnClickListener{ view ->
            Navigation.findNavController(view).navigate(R.id.action_pregunta1Fragment_to_segundoFragment)
            Toast.makeText(activity, "Respuesta Incorrecta", Toast.LENGTH_SHORT).show()

        }
        botonver2.setOnClickListener{ view->
            Navigation.findNavController(view).navigate(SegundoFragmentDirections.actionSegundoFragmentToPregunta3())
            Toast.makeText(activity, "Respuesta Correcta", Toast.LENGTH_SHORT).show()
        }
        return binding
    }

}