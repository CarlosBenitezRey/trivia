package com.example.pruebafragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.Navigation

class Pregunta1Fragment : Fragment() {
    private lateinit var botonfal: Button
    private lateinit var botonver: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding= inflater.inflate(R.layout.fragment_pregunta1, container, false)
        botonfal= binding.findViewById(R.id.botonfalso)
        botonver= binding.findViewById(R.id.botonverdadero)

        //arguments
        arguments?.let {
            var args =Pregunta1FragmentArgs.fromBundle(it)
            Toast.makeText(context,
                "Numero de preguntas correctas: ${args.cont}",
                Toast.LENGTH_LONG).show()
        }

        botonfal.setOnClickListener{ view ->
            Navigation.findNavController(view).navigate(R.id.action_pregunta1Fragment_to_segundoFragment)
            Toast.makeText(activity, "Respuesta Incorrecta", Toast.LENGTH_SHORT).show()

        }


        botonver.setOnClickListener{ view->
            Navigation.findNavController(view).navigate(Pregunta1FragmentDirections.actionPregunta1FragmentToSegundoFragment())
            Toast.makeText(activity, "Respuesta Correcta", Toast.LENGTH_SHORT).show()
        }
        return binding
    }


}